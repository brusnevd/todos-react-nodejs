const express = require("express");
const bodyParser = require("body-parser");
var pgp = require("pg-promise")( /*options*/ );
const config = require("config");
var cors = require('cors')


const app = express();
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const jsonParser = express.json();

const PORT = config.get('port') || 3000
const postgres = config.get('postgresUri')

app.set("view engine", "hbs");

async function start() {
    try {
        const db = (await pgp(postgres));

        app.use(cors());

        app.get("/", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            db.manyOrNone("SELECT * FROM tasks")
                .then(function(data) {
                    // console.log(data)
                    res.send(data);
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.post("/addTask", jsonParser, function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            console.log(req.body, "-------------------------------------------")

            let id = req.body.id;
            let text = req.body.text;
            let place = req.body.place;
            let discription = req.body.discription;
            let check = req.body.checked;

            db.none(`INSERT INTO tasks (id, text, place, discription) VALUES ('${id}', '${text}', '${place}', '${discription}')`)
                .then(function(data) {
                    res.send();
                    // res.redirect("http://localhost:8080/");
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.get("/delete/:id", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            // console.log(req.body, "-------------------------------------------")

            let id = req.params.id;

            console.log('ddd')

            db.none(`DELETE FROM tasks WHERE id=${id}`)
                .then(function(data) {
                    res.send();
                    // res.redirect("http://localhost:8080/");
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.get("/changeTask/:id", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');
            // res.set('Access-Control-Allow-Origin', 'http://localhost:8080');
            // res.set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');
            // res.set('Access-Control-Allow-Headers', '*');
            // res.set('Access-Control-Expose-Headers', '*');

            // console.log(req.body, "-----------qweqweqwe--------------------------------")

            let id = req.params.id;
            // let id = req.body.id;
            // let text = req.body.tasks.text;
            // let place = req.body.tasks.place;
            // let discription = req.body.tasks.discription;
            // let check = req.body.tasks.check;

            db.one(`SELECT checked FROM tasks WHERE id=${id}`)
                .then(function(data) {
                    db.none(`UPDATE tasks SET checked=${!data.checked} WHERE id=${id}`)
                        .then(function(data) {
                            res.send();
                            // res.redirect("http://localhost:8080/");
                        })
                        .catch(function(error) {
                            // res.send();
                            console.log("ERROR:", error);
                        });
                }).catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.post("/changeFullTask/:id", jsonParser, function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');
            // res.set('Access-Control-Allow-Origin', 'http://localhost:8080');
            // res.set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');
            // res.set('Access-Control-Allow-Headers', '*');
            // res.set('Access-Control-Expose-Headers', '*');

            console.log(req.body, "-----------qweqweqwe--------------------------------")

            let id = req.params.id;
            let text = req.body.text;
            let place = req.body.place;
            let discription = req.body.discription;
            // let id = req.body.id;
            // let text = req.body.tasks.text;
            // let place = req.body.tasks.place;
            // let discription = req.body.tasks.discription;
            // let check = req.body.tasks.check;

            db.none(`UPDATE tasks SET id=${id}, text='${text}', place='${place}', discription='${discription}' WHERE id=${id}`)
                .then(function(data) {
                    res.send();
                    // res.redirect("http://localhost:8080/");
                }).catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.get("/deleteTasks/:type", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');
            // res.set('Access-Control-Allow-Origin', 'http://localhost:8080');
            // res.set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');
            // res.set('Access-Control-Allow-Headers', '*');
            // res.set('Access-Control-Expose-Headers', '*');

            console.log(req.body, "-----------DELETE--------------------------------")

            let type = req.params.type;

            console.log(type)

            if (type == "All") {
                db.none(`DELETE FROM tasks`)
                    .then(function(data) {
                        res.send();
                        // console.log/
                        // res.redirect("http://localhost:8080/");
                    }).catch(function(error) {
                        console.log("ERROR:", error);
                    });
            } else if (type == "Active") {
                db.none(`DELETE FROM tasks WHERE checked=false`)
                    .then(function(data) {
                        res.send();
                        // console.log/
                        // res.redirect("http://localhost:8080/");
                    }).catch(function(error) {
                        console.log("ERROR:", error);
                    });
            } else if (type == "Completed") {
                db.none(`DELETE FROM tasks WHERE checked=true`)
                    .then(function(data) {
                        // console.log/
                        res.send();
                        // res.redirect("http://localhost:8080/");
                    }).catch(function(error) {
                        console.log("ERROR:", error);
                    });
            }

        })

        // app.post("/delete/:id", urlencodedParser, function(req, res) {
        //     res.set('Access-Control-Allow-Origin', '*');

        //     console.log(req.body, "-------------------------------------------")

        //     let id = req.params.id;

        //     db.none(`DELETE FROM tasks WHERE id=${id}`)
        //         .then(function(data) {
        //             // res.send(data);
        //             res.redirect("http://localhost:8080/");
        //         })
        //         .catch(function(error) {
        //             console.log("ERROR:", error);
        //         });
        // })

        app.listen(PORT, function() {
            console.log("Сервер ожидает подключения...");
        });
    } catch (e) {
        console.log("Server error", e.message);
        process.exit(1);
    }
}

// app.get("/", function(req, res) {
//     pool.query("SELECT * FROM users", function(err, data) {
//         if (err) return console.log(err);
//         res.render("index.hbs", {
//             users: data
//         });
//     });
// });

start();