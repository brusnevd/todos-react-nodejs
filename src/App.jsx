import React, { useState, useEffect } from 'react';
import Input from "./components/input/Input.jsx";
import Tasks from "./components/tasks/Tasks.jsx";
import Buttons from "./components/buttons/buttons.jsx";
import { saveToLocalStorage } from "./localstorage";
import {connect} from "react-redux"
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import More from './components/tasks/btn-more/More.jsx';
import { addTask, changeText, changePlace, changeDiscription, changeCheck, deleteTask, changeBtn, changeCount, changeState } from './redux/actions'

function App(props) {
  let id = props.config.id;

  return (
    <div className="container">
      <Router>
        <Route path='/' exact render={(propses) => <Input id={id} {...propses} {...props} />}></Route>
        <Route path='/' exact render={(propses) => <Tasks id={id} {...propses} {...props} />}></Route>
        <Route path='/more/:id' exact render={(propses) => <More {...propses} {...props} ></More>}></Route>
      </Router>
      <Buttons id={id} {...props} />
    </div>
  );
}

function mapStateToProps(state) {
  return state;
}

let mapDispatchToProps = {
  addTask,
  changeText,
  changePlace,
  changeDiscription,
  changeCheck,
  deleteTask,
  changeBtn,
  changeCount,
  changeState
}

export default connect(mapStateToProps, mapDispatchToProps)(App);