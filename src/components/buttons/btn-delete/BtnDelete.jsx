import React from 'react';
import classes from "./btn-delete.module.css"
import { saveToLocalStorage } from "./../../../localstorage";

function deleteTasks(props, url) {
    if (!confirm("Sure?")) {
        return;
    }

    fetch(url)
      .then(
        () => {
        //   setIsLoaded(true);
        //   setItems(result);
            // props.deleteTask("del" + taskId);
            let mainTasks = props.store.getState().tasks;

            if (props.activeBtn === "All") {
                mainTasks.forEach(element => {
                    props.deleteTask('del' + element.id);
                });
            }

            if (props.activeBtn === "Active") {           
                mainTasks.forEach(element => {
                    if (element.checked == false) {
                        props.deleteTask('del' + element.id);
                    }
                });
            }

            if (props.activeBtn === "Completed") {
                mainTasks.forEach(element => {
                    if (element.checked == true) {
                        props.deleteTask('del' + element.id);
                    }
                });
            }
        },
        // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
        // чтобы не перехватывать исключения из ошибок в самих компонентах.
        (error) => {
        //   setIsLoaded(true);
        //   setError(error);
            console.log(error);
        }
      )

    // saveToLocalStorage(props.id, props.store.getState().tasks);

    // props.changeCount(0);
    // props.changeBtn('O');
}

const BtnDelete = (props) => {
    let text = "";
    if (['All', 'Active', 'Completed'].indexOf(props.activeBtn) + 1) {
        text = "Delete " + props.activeBtn + " tasks";
    }
    let url = "http://localhost:3000/deleteTasks/" + props.activeBtn;
    // console.log(props.activeBtn)
    return (
        <div action={url} method="POST" className={classes.btnDelete}>
            {/* <input type="hidden" name="btn" value={props.activeBtn}/> */}
            <input type="submit" onClick={() => deleteTasks(props, url)} value={text} />
        </div>
    )
}

export default BtnDelete;