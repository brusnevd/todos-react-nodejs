import React from 'react';
import classes from "./btns-show.module.css"
import BtnAll from "./btn-all/BtnAll.jsx";
import BtnActive from "./btn-active/BtnActive.jsx";
import BtnCompleted from "./btn-completed/BtnCompleted.jsx";

const BtnsShowInfo = (props) => {
    return (
        <div className={classes.btnsShowInfo}>
            <BtnAll {...props} />
            <BtnActive {...props} />
            <BtnCompleted {...props} />
        </div>
    )
}

export default BtnsShowInfo;