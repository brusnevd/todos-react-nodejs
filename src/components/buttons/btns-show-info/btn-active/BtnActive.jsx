import React from 'react';

function activeClickHandler(props) {
    let mainState = props.store.getState();

    if (mainState.activeBtn === 'Active') {
        props.changeBtn('O');
        props.changeCount(0);
    } else {
        let count = mainState.tasks.reduce((count, current) => count + !current.checked, 0);
        props.changeBtn('Active');
        props.changeCount(count);
    }
}

const BtnActive = (props) => {
    return (
        <div onClick={() => activeClickHandler(props)}>
            Active
        </div>
    )
}

export default BtnActive;