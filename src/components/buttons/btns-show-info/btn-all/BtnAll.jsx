import React from 'react';

function allClickHandler(props) {
    let mainState = props.store.getState();

    if (mainState.activeBtn === 'All') {
        props.changeBtn('O');
        props.changeCount(0);
    } else {
        props.changeBtn('All');
        props.changeCount(mainState.tasks.length);
    }
}

const BtnAll = (props) => {
    return (
        <div onClick={() => allClickHandler(props)}>
            All
        </div>
    )
}

export default BtnAll;