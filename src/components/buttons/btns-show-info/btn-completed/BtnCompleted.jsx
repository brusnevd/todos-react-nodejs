import React from 'react';

function completedClickHandler(props) {
    let mainState = props.store.getState();

    if (mainState.activeBtn === 'Completed') {
        props.changeBtn('O');
        props.changeCount(0);
    } else {
        let count = mainState.tasks.reduce((count, current) => count + !!current.checked, 0);
        props.changeBtn('Completed');
        props.changeCount(count);
    }
}

const BtnCompleted = (props) => {
    return (
        <div onClick={() => completedClickHandler(props)}>
            Completed
        </div>
    )
}

export default BtnCompleted;