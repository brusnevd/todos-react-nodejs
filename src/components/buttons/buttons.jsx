import React from 'react';
import classes from "./buttons.module.css"
import BtnInfo from "./btn-info/BtnInfo.jsx";
import BtnDelete from "./btn-delete/BtnDelete.jsx";
import BtnsShowInfo from "./btns-show-info/BtnsShowInfo.jsx";

const Buttons = (props) => {
    return (
        <div className={classes.buttonsContainer}>
            <BtnInfo {...props} />
            <BtnsShowInfo {...props} />
            <BtnDelete {...props} />
        </div>
    )
}

export default Buttons;