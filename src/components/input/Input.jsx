import React, { useEffect, useState } from "react";
import classes from "./input.module.css";

import { saveToLocalStorage } from "./../../localstorage";


function keyUpHandler(props, event, id) {
    if (event.keyCode !== 13) {
        let targetValue = event.target.value;

        props.changeText(targetValue);
    }
}

function placeInputHandler(props, event, id) {
    if (event.keyCode !== 13) {

        let targetValue = event.target.value;

        props.changePlace(targetValue);
    }
}

function discriptInputHandler(props, event, id) {
    if (event.keyCode !== 13) {
        let targetValue = event.target.value;

        props.changeDiscription(targetValue);
    }
}

function addTaskBtnHandler(props, event, id) {
    let mas = props.store.getState().tasks;

    let task = {
        text: props.text,
        place: props.place,
        discription: props.discription,
        checked: false,
        id: mas.length !== 0 ? Math.max(...(mas.map((current) => current.id))) + 1 : 0,
    };

    // props.addTask(task);

    mas = props.store.getState().tasks;
    
    // , {
        // method: "POST",
        // headers: {
        //     'Content-Type': 'application/json;charset=utf-8',
        //     'Access-Control-Allow-Origin': '*'
        // },
        // body: JSON.stringify(task)
    // }

    // saveToLocalStorage(id, mas);

    fetch("http://localhost:3000/addTask/", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(task)
      })
      .then(
        () => {
        //   setIsLoaded(true);
        //   setItems(result);
            // props.deleteTask("del" + taskId);
            props.addTask(task);
        },
        // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
        // чтобы не перехватывать исключения из ошибок в самих компонентах.
        (error) => {
        //   setIsLoaded(true);
        //   setError(error);
            console.log(error);
        }
      )
}

const Input = (props) => {
    let mas = props.store.getState().tasks;

    let task = {
        text: props.text,
        place: props.place,
        discription: props.discription,
        check: false,
        id: mas.length !== 0 ? Math.max(...(mas.map((current) => current.id))) + 1 : 0,
    };
    return (
        <div className={classes.inputContainer} action="http://localhost:3000/addTask" method="POST">
            {/* <input type="hidden" name="id" value={task.id} /> */}
            {/* <input type="hidden" name="id" value={task.id} /> */}
            <input name="text" type="text" className={classes.input} placeholder="Enter task" onKeyUp={ (event) => { keyUpHandler(props, event, props.id) } } />
            <input name="place" type="text" className={classes.input} placeholder="Enter place" onKeyUp={ (event) => { placeInputHandler(props, event, props.id) } } />
            <input name="discription" type="text" className={classes.input} placeholder="Enter description" onKeyUp={ (event) => { discriptInputHandler(props, event, props.id) } } />
            <input className={classes.btnAdd} type="submit" onClick={ (event) => {  addTaskBtnHandler(props, event, props.id) } } />
        </div>
    )
}

export default Input;