import React, { useState, useEffect } from 'react';
import classes from "./tasks.module.css"
import More from "./btn-more/More.jsx";
import { saveToLocalStorage } from "./../../localstorage";
import {
    BrowserRouter as Router,
    NavLink
} from "react-router-dom";
let throught = false;

function deleteTask(event, props, taskId) {

    fetch("http://localhost:3000/delete/" + taskId)
      .then(
        () => {
        //   setIsLoaded(true);
        //   setItems(result);
            props.deleteTask("del" + taskId);
        },
        // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
        // чтобы не перехватывать исключения из ошибок в самих компонентах.
        (error) => {
        //   setIsLoaded(true);
        //   setError(error);
            console.log(error);
        }
      )

    // saveToLocalStorage(props.id, props.store.getState().tasks);
}

function checkboxChange(event, props) {
    let tasks = props.store.getState().tasks;

    for (let i = 0; i < props.store.getState().tasks.length; i++) {
        console.log(event.target.id.toString(), tasks[i].id.toString())
        if ('checkbox' + tasks[i].id.toString() == event.target.id.toString()) {
            fetch('http://localhost:3000/changeTask/' + tasks[i].id.toString())
                .then(
                    () => {
                    //   setIsLoaded(true);
                    //   setItems(result);
                        // props.deleteTask("del" + taskId);
                        props.changeCheck('checkbox' + tasks[i].id.toString());
                    },
                    // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
                    // чтобы не перехватывать исключения из ошибок в самих компонентах.
                    (error) => {
                    //   setIsLoaded(true);
                    //   setError(error);
                        console.log(error);
                    }
                )


            // let xhr = new XMLHttpRequest();
            // xhr.withCredentials = true;

            // let json = JSON.stringify(tasks[i]);

            // xhr.open("POST", 'http://localhost:3000/changeTask/' + tasks[i].id.toString());
            // xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            // xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            // throught = !throught;
            // console.log(throught)

            // xhr.send();

            break;
        }
    }

    // saveToLocalStorage(props.id, props.store.getState().tasks);
}

const Tasks = (props) => {
    const [isLoaded, setIsLoaded] = useState(false);

    // console.log("render")
    
    let tasksInit;
    useEffect(() => {
        fetch("http://localhost:3000/")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    // console.log(result)
                    props.changeState(result);
                    // result.forEach(element => {
                    //     props.addTask(element);
                    // });
                },
                (error) => {
                    setIsLoaded(true);
                    console.log(error)
                }
            )
    }, [])

    if (!isLoaded) {
        return(
            <div>Loading...</div>
        )
    } else {
        let mainState = props.store.getState();
        let mainTasks = props.store.getState().tasks;
        console.log('taskspropsp', mainTasks)
        mainTasks.sort((a, b) => a.id > b.id ? 1 : -1);
        let highClass = false;
        let tasksMas = mainTasks.map((current) => {
            if (mainState.activeBtn === 'All') {
                highClass = true;
            }
            if (mainState.activeBtn === 'Active') {
                if (current.checked) {
                    highClass = false;
                } else {
                    highClass = true;
                }
            }
            if (mainState.activeBtn === 'Completed') {
                if (current.checked) {
                    highClass = true;
                } else {
                    highClass = false;
                }
            }
            return (
                <div key={current.id} className={classes.task + (highClass === true ? " " + classes.high : "")} >
                    <input type="checkbox" id={"checkbox" + current.id} className={classes.checkbox} defaultChecked={current.checked} onClick={(event) => checkboxChange(event, props)} />
                    <label htmlFor={"checkbox" + current.id} className={classes.taskText + (current.checked ? " " + classes.through : "")} onClick={(event) => checkboxChange(event, props)} > {current.text} </label>
                    {/* <form action={"http://localhost:3000/delete/" + current.id} method="POST" className={classes.formDelete}>
                        <input type="hidden" /> */}
                        <div className={classes.delete + " " + classes.deleteInput} onClick={(event) => deleteTask(event, props, current.id)} id={"del" + current.id}>&#10006;</div>
                    {/* </form> */}
                    <NavLink to={'/more/' + current.id}>
                        <span className={classes.more} id={"more" + current.id} title="More">
                            <span className={classes.dot}></span>
                            <span className={classes.dot}></span>
                            <span className={classes.dot}></span>
                        </span>
                    </NavLink>
                </div>
            )});

        return (
            <div className={classes.tasksContainer}>
                {tasksMas}
            </div>
        )
    }
}

export default Tasks;