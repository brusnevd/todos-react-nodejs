import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import classes from "./more.module.css"

function editClickHandler(event, read, setRead, setSrc, src, props, taskId) {
    if (src == "../public/ok.png") {
        let type = event.target.alt;
        let val = document.getElementById(event.target.alt).value;
        let text = document.getElementById('text').value;
        let place = document.getElementById('place').value;
        let discription = document.getElementById('discription').value;

        let task = {
            text: text,
            place: place,
            discription: discription
        }

        fetch("http://localhost:3000/changeFullTask/" + taskId, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(task)
          })
            .then(
                () => {
                //   setIsLoaded(true);
                //   setItems(result);
                    // props.deleteTask("del" + taskId);
                    if (type == "text") {
                        props.changeText(document.getElementById(val));
                    } else if (type == 'place') {
                        props.changePlace(document.getElementById(val));
                    } else if (type == "discription") {
                        props.changeDiscription(document.getElementById(val));
                    }

                },
                // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
                // чтобы не перехватывать исключения из ошибок в самих компонентах.
                (error) => {
                //   setIsLoaded(true);
                //   setError(error);
                    console.log(error);
                }
            )
    }
    setRead(!read);
    setSrc(src == "../public/ok.png" ? "../public/edit.png" : "../public/ok.png");
}

function More(props) {
    console.log("more - ", props)
    let [read1, setRead1] = useState(true);
    let [read2, setRead2] = useState(true);
    let [read3, setRead3] = useState(true);

    let [imgSrc1, setSrc1] = useState("../public/edit.png");
    let [imgSrc2, setSrc2] = useState("../public/edit.png");
    let [imgSrc3, setSrc3] = useState("../public/edit.png");

    let mainState = props.store.getState();

    let place,
        discription,
        taskText

    for (let i = 0; i < mainState.tasks.length; i++) {
        if (props.match.params.id.toString() === mainState.tasks[i].id.toString()) {
            place = mainState.tasks[i].place;
            discription = mainState.tasks[i].discription;
            taskText = mainState.tasks[i].text;
            break;
        }
    }
    
    return (
        <div className={classes.more}>
            <NavLink to='/'>
                <div className={classes.homeBtn}>HOME</div>
            </NavLink>
            {/* <form action={"http://localhost:3000/changeFullTask/" + props.match.params.id } method="POST" > */}
                <div className={classes.taskText}>
                    {/* <form action={"http://localhost:3000/changeTask/" + props.match.params.id} method="POST"> */}
                        {/* <input type="submit" className={classes.edit} value="OK" /> */}
                            <img src={imgSrc1} alt="text" className={classes.edit} onClick={(event) => editClickHandler(event, read1, setRead1, setSrc1, imgSrc1, props, props.match.params.id)} />
                            {/* <input type="hidden" name="" /> */}
                            <textarea readOnly={read1} name="text" id="text">{taskText}</textarea>
                    {/* </form> */}
                </div>
                <div className={classes["place-label"]}>Place</div>
                <div className={classes.place}>
                    <img src={imgSrc2} alt="place" className={classes.edit} onClick={(event) => editClickHandler(event, read2, setRead2, setSrc2, imgSrc2, props, props.match.params.id)} />
                    <textarea readOnly={read2} name="place" id="place">{place}</textarea>
                </div>
                <div className={classes["discription-label"]}>discription</div>
                <div className={classes.discription}>
                    <img src={imgSrc3} alt="discription" className={classes.edit} onClick={(event) => editClickHandler(event, read3, setRead3, setSrc3, imgSrc3, props, props.match.params.id)} />
                    <textarea readOnly={read3} name="discription" id="discription">{discription}</textarea>
                </div>

                {/* <input type="submit" name="" id="subForm" className={classes.subForm} /> */}
            {/* </form> */}
        </div>
    );
}

export default More;