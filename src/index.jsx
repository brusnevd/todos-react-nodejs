import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import {createStore} from "redux";
import {connect, Provider} from "react-redux";
import rootReducer from "./redux/rootReducer.js";

let config = {
    id: 1,
}, tasksInit = []

// let tasks = localStorage.getItem("list" + config.id);

let store = createStore(rootReducer, {
    tasks: tasksInit,
    text: "",
});

const app = (
    <Provider store={store}>
        <App config={config} store={store} />
    </Provider>
)

ReactDOM.render(
    app,
    document.getElementById('root')
);