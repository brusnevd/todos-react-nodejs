import { ADDTASK, CHANGEDISCRIPTION, CHANGETEXT, CHANGEPLACE, CHANGECHECK, DELETETASK, CHANGEBTN, CHANGECOUNT, CHANGESTATE } from "./types";

export function addTask(task) {
    return {
        type: ADDTASK,
        payload: task
    }
}

export function changeState(state) {
    return {
        type: CHANGESTATE,
        payload: state
    }
}

export function changeText(letter) {
    return {
        type: CHANGETEXT,
        payload: letter
    }
}

export function changePlace(letter) {
    return {
        type: CHANGEPLACE,
        payload: letter
    }
}

export function changeDiscription(letter) {
    return {
        type: CHANGEDISCRIPTION,
        payload: letter
    }
}

export function changeCheck(identif) {
    return {
        type: CHANGECHECK,
        payload: identif
    }
}

export function deleteTask(identif) {
    return {
        type: DELETETASK,
        payload: identif
    }
}

export function changeBtn(title) {
    return {
        type: CHANGEBTN,
        payload: title
    }
}

export function changeCount(count) {
    return {
        type: CHANGECOUNT,
        payload: count
    }
}