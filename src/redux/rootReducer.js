import { changeText } from "./actions";
import { ADDTASK, CHANGETEXT, CHANGEPLACE, CHANGEDISCRIPTION, CHANGECHECK, DELETETASK, CHANGEBTN, CHANGECOUNT, CHANGESTATE } from "./types";

const initialState = {
    tasks: {},
    text: '',
    place: '',
    discription: '',
    activeBtn: '0',
    count: 0
}

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGESTATE:
            return {
                ...state,
                tasks: action.payload
            }
        case ADDTASK:
            return {
                ...state,
                tasks: [...state.tasks, action.payload]
            }
        case CHANGETEXT:
            return {
                ...state,
                text: action.payload
            }
        case CHANGEPLACE:
            return {
                ...state,
                place: action.payload
            }
        case CHANGEDISCRIPTION:
            return {
                ...state,
                discription: action.payload
            }
        case CHANGECHECK:
            let tasks = state.tasks.map((current) => {
                if ("checkbox" + current.id === action.payload) {
                    if (current.checked === false) {
                        current.checked = true;
                    } else {
                        current.checked = false;
                    }
                }
                return current;
            });
            return {
                ...state,
                tasks
            }
        case DELETETASK:
            tasks = state.tasks.filter((current) => {
                return "del" + current.id.toString() !== action.payload.toString();
            });

            console.log("taskssssssss - ", action.payload)
            return {
                ...state,
                tasks
            }
        case CHANGEBTN:
            return {
                ...state,
                activeBtn: action.payload
            }
        case CHANGECOUNT:
            return {
                ...state,
                count: action.payload
            }
        default:
            return state;
    }
}